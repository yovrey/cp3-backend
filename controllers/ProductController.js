const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")


// CREATE A PRODUCT ---- ADMIN ONLY
module.exports.createProduct = (data) => {

		let newProduct = new Product ({
			name : data.name,
			description : data.description,
			price : data.price
		})
		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return {message: "New product created successfully!"}
			}
		})
}




// GET PRODUCT DETAILS
module.exports.getProductDetails = (productId) => {
	return Product.findById(productId).then((result, error) => {
			return result		
	})
}

// GET ALL PRODUCTS 
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}


// GET ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}



// UPDATE A SINGLE PRODUCT -----ADMIN ONLY
module.exports.updateProduct = (productId, data) => {
	return Product.findByIdAndUpdate(productId, {
		name : data.name,
		description : data.description,
		price : data.price
	}).then((updatedProduct, error) => {
		if(error){
			return false;
		}
		else{
			return {message: "The product has been updated!"}
		}
	})		
}



// ARCHIVING A PRODUCT -----ADMIN ONLY
module.exports.archiveProduct =(productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive : false
	}).then((archivedProduct, error) => {
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

// RESTORE A PRODUCT -----ADMIN ONLY
module.exports.restoreProduct =(productId) => {
	return Product.findByIdAndUpdate(productId, {
		isActive : true
	}).then((archivedProduct, error) => {
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}

