const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")



// Check if email exist
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0) {
			return true
		}

		return false
	})
}



// User registration
module.exports.register = (data) => {

	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password

	})

	return new_user.save().then((created_user, error) => {
		if(error) {
			return false
		}

		return {
			message: "You have successfully registered. Welcome to our store!"
		}
	})
}



// User login/authentication
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null) {
			return {
				messageToken: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
			return { accessToken: auth.createAccessToken(result) }
		}

		return { message: "Password is incorrect!" }
	})
}



// PASTE TO CONTROLLER
// Get single user based on ID from token
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Makes the password not be included in the result
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});
};



// Set user as Admin ---- ADMIN ONLY
module.exports.makeAdmin = async (user, request) => {

	if(user.isAdmin){	

		return User.findByIdAndUpdate(request.userId, { isAdmin: true }).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return ('User is now an admin!')
			}
		})
	}
	else{
		return (`You have no access!`)
	}
}

module.exports.getUserDetails = (userId) => {
	return User.findById(userId, {password: 0}).then((result, error) => {
		if(error) {
			return false
		}
		else {
			return result
		}
	})
}

