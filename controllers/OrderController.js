const User = require("../models/User")
const Order = require ("../models/Order")
const Product = require ("../models/Product")

const bcrypt = require("bcrypt")
const auth = require("../auth.js")


// Add to cart -----NON ADMIN ONLY
module.exports.checkout = async (data) => {
	if(data.isAdmin) {
		return Promise.resolve({
			message: "You are no allowed to create an order!"
		})
	}

	let product = await Product.findOne({_id: data.order.productId}).then(result => {
		return result
	})
	
	let new_order = new Order({
		userId: data.order.userId,		
		productId: data.order.productId,
		productName: product.name,
		quantity: data.order.quantity,		
		totalAmount: product.price*data.order.quantity,
		purchasedOn: data.order.purchasedOn
	})

	return new_order.save().then((new_order, error) => {
		if (error) {
			return false
		}
		return true
	})
}



// From cart to order
module.exports.addOrder = async (data) => {
	console.log(data)
	
	let product = await Product.findOne({_id: data.order.productId}).then(result => {
		return result
		console.log(result)
	})

	// Add the product name in the orders array of the user
	User.findOne({_id: data.order.userId}).then(result => {
		console.log(result)
		let orderSummary = {
			productName: product.name,
			totalAmount: product.price*data.order.quantity,
			quantity: data.order.quantity
		}

		result.orders.push(orderSummary)

		return result.save().then((result, error) => {
			if (error) {
			return false
			}
			return true
		})
	})
}



// Get all orders -----ADMIN ONLY
module.exports.getOrders = async (user) => {	
	return Order.find({isOrder: true}).then((result) => {
		return result
	})
}



// Get Users all orders ------ 
module.exports.all = async (user) => {
	return Order.find({userId:user.id, isOrder: true})
	.then(result => {
		return result
	})	
}

// Get cart Items ------ 
module.exports.cart = async (user) => {
	return Order.find({userId:user.id, isOrder: false})
	.then(result => { 
		return result
	})	
}

// Add to Order from Cart
module.exports.addToOrder =(orderId) => {
	return Order.findByIdAndUpdate(orderId, {
		isOrder : true
	}).then((orderedProduct, error) => {
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}


// Get single order ------ 
module.exports.orderDetails =  (orderId) => {
	return Order.findById(orderId).then(result => { 
			return result
	})	
}

// Update order
module.exports.updateOrder = async (orderId, data) => {

	let order = await Order.findOne({_id:orderId}).then(result => {
		console.log(result)
		return result
	})
	let price = order.totalAmount/order.quantity
	console.log(price)

	return Order.findByIdAndUpdate(orderId, {
		quantity : data.quantity,
		totalAmount: price*data.quantity
	}).then((updatedOrder, error) => {
		if(error){
			return false;
		}
		else{
			return {message: "Order has been updated!"}
		}
	})		

}






// Delete order
module.exports.deleteOrder = (orderId) => {
	return Order.findByIdAndRemove(orderId).then((result, error) => {
		if(error){
			return false
		}
		else {
			return true
		}
	})
}





