const mongoose = require("mongoose")

const cart_schema = new mongoose.Schema({

	productId: {
		type: String,
		required: [ true, "Profuct ID is required"]
	},
	productName: {
		type: String,
		required: [true, "product name"]
	},
	quantity: {
		type: Number,
		required: [true, "Quantity is required!"]
	},

	Subtotal: {
		type: Number,
		required: [true, "Total amount is required!"]
	},
	orderedOn: {
		type: Date,
		default: new Date()
	}
})

 
module.exports = mongoose.model("Cart", cart_schema)