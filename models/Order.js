const mongoose = require("mongoose")

const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required!"]
	},

	productId: {
		type: String,
		required: [ true, "Profuct ID is required"]
	},
	productName: {
		type: String,
		required: [true, "product name"]
	},
	quantity: {
		type: Number,
		required: [true, "Quantity is required!"]
	},

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required!"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	isOrder: {
		type: Boolean,
		default: false
	}
})

 
module.exports = mongoose.model("Order", order_schema)
