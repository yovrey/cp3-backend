const express = require("express")
const router = express.Router()

const ProductController = require("../controllers/ProductController")
const auth = require("../auth")


// Create a product -----ADMIN ONLY
router.post("/create-product",(request, response) => {
	ProductController.createProduct(request.body).then((result) => {
		response.send(result)
	})
})



// Get single product details
router.get("/:id/product-details", (request, response) => {
	ProductController.getProductDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Get all products
router.get("/", (request, response) => {
	ProductController.getAllProducts().then((result) => {
		response.send(result)
	})
})



// Get all active products
router.get("/active", (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	})
})


// Update a product details -----ADMIN ONLY
router.patch("/:id/update", (request, response) => {
	ProductController.updateProduct(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Archiving a product ----ADMIN ONLY
router.patch("/:id/archive",(request, response) => {
	ProductController.archiveProduct(request.params.id).then((result) => {
		response.send(result)
	})
})

// Restore a product -----ADMIN ONLY
router.patch("/:id/restore",(request, response) => {
	ProductController.restoreProduct(request.params.id).then((result) => {
		response.send(result)
	})
})




module.exports = router