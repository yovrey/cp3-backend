
const express = require("express")
const router = express.Router()

const UserController = require("../controllers/UserController")
const auth = require("../auth")

// Check if email already exist in the database
router.post("/check-email", (request, response) => {
	UserController.checkIfEmailExists(request.body).then((result) => {
		response.send(result)
	})
})


// Register a user
router.post("/register", (request, response) => {
	UserController.register(request.body).then((result) => {
		response.send(result)
	})
})


// Login user
router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})


// PASTE TO ROUTES
// Get user details from token
router.get("/details", auth.verify, (request, response) => {

	// Retrieves the user data from the token
	const user_data = auth.decode(request.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	UserController.getProfile({userId : user_data.id}).then(result => response.send(result));

});


// Set user as Admin ------- yhong@email.com, admin123 
router.patch("/:userId/make-admin", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	UserController.makeAdmin(user, request.params, request.body).then((result) => {
		response.send(result)
	})
})

// Get single user details
router.get("/:id", (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		console.log(result.orders)
		response.send(result)
	})
})




module.exports = router
