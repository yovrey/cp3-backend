const express = require("express")
const router = express.Router()
const OrderController = require("../controllers/OrderController")
const auth = require("../auth")


// Create order -----NON ADMIN ONLY
router.post("/checkout", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	const data = {
		order: request.body
	}
	console.log(request.body)
	OrderController.checkout(data).then((result) => {
		response.send(result)
	})
})

// Place order
router.patch("/place-order", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)
	const data = {
		order: request.body
	}
	// console.log(data)
	OrderController.addOrder(data).then((result) => {
		response.send(result)
	})
})



// Get all orders -----ADMIN ONLY
router.get("/", (request, response) => {
	OrderController.getOrders().then((result) => {
		response.send(result)
	})
})



  // Get user's all orders -------
router.get("/user-orders", auth.verify, (request,response)=>{
	const user = auth.decode(request.headers.authorization)
	OrderController.all(user).then((result) => {
			response.send(result)
	})
})



  // Get user's all orders -------
router.get("/user-cart", auth.verify, (request,response)=>{
	const user = auth.decode(request.headers.authorization)
	OrderController.cart(user).then((result) => {
			response.send(result)
	})
})

// Add to Order From Cart
router.patch("/:id/add-order", (request, response) => {
	OrderController.addToOrder(request.params.id, request.body).then((result) => {
		console.log(result)
		response.send(result)
	})
})



  // Get single orders -------
router.get("/:id", (request,response)=>{
	OrderController.orderDetails(request.params.id).then((result) => {
			response.send(result)
	})
})

// Update a order 
router.patch("/:id/update-order", (request, response) => {
	OrderController.updateOrder(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})




// Delete order
router.delete("/:id/delete", (request, response) => {
	OrderController.deleteOrder(request.params.id).then((result) => {
		response.send(result)
	})
})






module.exports = router 